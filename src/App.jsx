import React, { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import axios from "axios";

import Header from "./components/Header";
import About from "./pages/About";
import Home from "./pages/Home";
import Memory from "./pages/Memory";

function App() {
  const [data, setData] = useState([]);
  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    var config = {
      method: "get",
      maxBodyLength: Infinity,
      url: "https://d51b9f31-a142-466f-801a-10e775e32851.mock.pstmn.io",
      headers: {},
    };

    axios(config)
      .then(function (response) {
        setData(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home data={data} />} />
        <Route path="/about" element={<About />} />
        <Route path="/memory" element={<Memory />} />
      </Routes>
    </div>
  );
}

export default App;
