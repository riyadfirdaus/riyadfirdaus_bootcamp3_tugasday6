import React from "react";
import style from "../assets/Home.module.css";
import CardComponent from "../components/CardComponent";
import Sidebar from "../components/Sidebar";

function Home(props) {
  const data = props.data;
  return (
    <div className={style.window}>
      <div className={style.cardList}>
        {data.map((data, index) => {
          return (
            <CardComponent
              key={index}
              author={data.author}
              url={data.img}
              title={data.title}
            />
          );
        })}
      </div>
      <div className={style.sidebar}>
        <Sidebar />
      </div>
    </div>
  );
}

export default Home;
