import { AppBar, Box, Button, Toolbar } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";
import CameraAltIcon from "@mui/icons-material/CameraAlt";

function Header() {
  return (
    <div>
      <AppBar position="sticky">
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <CameraAltIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            <Button
              component={NavLink}
              to="/"
              sx={{ my: 2, color: "white", display: "block" }}
            >
              My App
            </Button>
            <Button
              component={NavLink}
              to="/about"
              sx={{ my: 2, color: "white", display: "block" }}
            >
              About
            </Button>
            <Button
              component={NavLink}
              to="/memory"
              sx={{ my: 2, color: "white", display: "block" }}
            >
              Memory
            </Button>
          </Box>

          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;
