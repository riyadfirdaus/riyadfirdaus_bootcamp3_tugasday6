import Avatar from "@mui/material/Avatar";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import { red } from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import React from "react";

function CardComponent(props) {
  return (
    <Card
      sx={{
        sx: 1.0, // 100%
        sm: 250,
        md: 350,
      }}
    >
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            R
          </Avatar>
        }
        title={props.title}
        subheader="March 15, 2023"
      />
      <CardMedia
        component="img"
        height="194"
        image={props.url + "?w=500&auto=compress"}
        alt="Paella dish"
        loading="lazy"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          Ini merupakan foto {props.title} yang diambil dari API Axios
        </Typography>
      </CardContent>
    </Card>
  );
}

export default CardComponent;
